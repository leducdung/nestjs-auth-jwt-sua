import * as https from 'https'
import { stringify as queryStringify } from 'querystring'
import { v1 as uuidv1 } from 'uuid';

export interface RequestOptions {
  url?: string
  localAddress?: string
  socketPath?: string
  method?: string
  headers?: Headers
  auth?: string
  params?: ParsedUrlQueryInput
  timeout?: number
  setHost?: boolean
  rejectUnauthorized?: boolean
  servername?: string
  body?: object
}
export interface Headers {
  [header: string]: number | string | string[] | undefined
}
export interface LiteralObject {
  [key: string]: string | string[] | number | object | undefined
}
export interface Response {
  status: number
  body: LiteralObject
}

interface ParsedUrlQueryInput extends NodeJS.Dict<string | number | boolean | ReadonlyArray<string> |
ReadonlyArray<number> | ReadonlyArray<boolean> | null> {
}

export const MethodsNeedToWriteData = ['POST', 'PUT']

export const sendHttpsRequest = async (): Promise<Response> => {
  return new Promise((resolve, reject) => {

    var endpoint = "https://test-payment.momo.vn/gw_payment/transactionProcessor"
      var hostname = "https://test-payment.momo.vn"
      var path = "/gw_payment/transactionProcessor"
      var partnerCode = "MOMOYTUU20210331"
      var accessKey = "K9rAYoI4cWBdK7fR"
      var serectkey = "gVwUvBnaIZJdXd9adUkEM2Cinp5PCjn8"
      var orderInfo = "pay with MoMo"
      var returnUrl = "https://momo.vn/return"
      var notifyurl = "https://callback.url/notify"
      var amount = "10000"
      var orderId = uuidv1()
      var requestId = uuidv1()
      var requestType = "captureMoMoWallet"
      var extraData = "merchantName;merchantId" //pass empty value if your merchant does not have stores else merchantName=[storeName]; merchantId=[storeId] to identify a transaction map with a physical store

      //before sign HMAC SHA256 with format
      //partnerCode=$partnerCode&accessKey=$accessKey&requestId=$requestId&amount=$amount&orderId=$oderId&orderInfo=$orderInfo&returnUrl=$returnUrl&notifyUrl=$notifyUrl&extraData=$extraData
      var rawSignature = "partnerCode=" + partnerCode + "&accessKey=" + accessKey + "&requestId=" + requestId + "&amount=" + amount + "&orderId=" + orderId + "&orderInfo=" + orderInfo + "&returnUrl=" + returnUrl + "&notifyUrl=" + notifyurl + "&extraData=" + extraData
      //puts raw signature
      console.log("--------------------RAW SIGNATURE----------------")
      console.log('rawSignature' + rawSignature)
      //signature
      var crypto = require('crypto');
      var signature = crypto.createHmac('sha256', serectkey)
        .update(rawSignature)
        .digest('hex');
      console.log("--------------------SIGNATURE----------------")

      console.log(signature)

      //json object send to MoMo endpoint
      var body = JSON.stringify({
        partnerCode: partnerCode,
        accessKey: accessKey,
        requestId: requestId,
        amount: amount,
        orderId: orderId,
        orderInfo: orderInfo,
        returnUrl: returnUrl,
        notifyUrl: notifyurl,
        extraData: extraData,
        requestType: requestType,
        signature: signature,
      })
      //Create the HTTPS objects
      var options = {
        hostname: 'test-payment.momo.vn',
        port: 443,
        path: '/gw_payment/transactionProcessor',
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Content-Length': Buffer.byteLength(body)
        }
      };
    const req = https.request(options, (res) => {
      const statusCode = res.statusCode || 500
      let responseBody = ''
      res.on('data', (chunk) => {
        responseBody += chunk
      })
      res.on('end', () => {
        { resolve({
          status: statusCode,
          body: JSON.parse(responseBody),
        })}
      })
    })
    req.on('error', (err) => {
      reject(err)
    })
    if (body) {
      req.write(body)
    }
    req.end()
  })
}

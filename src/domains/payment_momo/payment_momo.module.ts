import { Module, forwardRef } from '@nestjs/common';
import { Payment_momoService } from './payment_momo.service';
import { Payment_momoController } from './payment_momo.controller';


@Module({
  // imports: [
  //   forwardRef(() => AuthModule),
  //   forwardRef(() => databaseModule),
  //   forwardRef(() => noticationModel),
  // ],
  controllers: [Payment_momoController],
  providers: [Payment_momoService],
  exports: [Payment_momoService],
})
export class Payment_momoModule {}

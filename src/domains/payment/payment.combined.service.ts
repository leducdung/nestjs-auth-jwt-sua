import { Injectable } from '@nestjs/common';
import { EmployeesService } from '../employees/employees.service';
import { UsersService } from '../users/users.service';
import { Users } from '../users/model/users.interface';
import { PaymentService } from "./payment.service";
import { FireBaseService } from "../fireBase/firebase.service";
import { Payment_momoService } from "../payment_momo/payment_momo.service";
import { PaymentStatus, statusProduct } from "../../constains/common";
import { ProductsService } from "../products/products.service";
import { StoreOwnersService } from "../storeOwners/storeOwners.service"
import { NoticationsService } from "../notications/notications.service"

@Injectable()
export class PaymentCombinedService {
  constructor(
    private readonly paymentService: PaymentService,
    private readonly payment_momoService: Payment_momoService,
    private readonly productsService: ProductsService,
    private readonly storeOwnersService: StoreOwnersService,
    private readonly usersService: UsersService,
    private readonly fireBaseService: FireBaseService,
    private readonly noticationsService: NoticationsService,
  ) { }


  async CreatePaymentHandle({ data }) {
    try {
      if (!data.userCreatedProductID) {
        return {
          message: 'Cần nhập userCreatedProductID nhé 2 bạn'
        }
      }

      if (!data.productIDs || data.productIDs.lenth === 0) {
        return {
          message: 'Cần có sản phẩm để thanh toán'
        }
      }

      const userCreatedProduct = await this.usersService.findOneUser({_id : data.userCreatedProductID})

      if(!userCreatedProduct){
        return {
          message: 'sai ID người tạo ra sản phẩm'
        }
      }

      const allProduct = await this.productsService.findMany({query:{
        createdBy: data.userCreatedProductID
      }})

      var canNotPayment = []

      allProduct.list.forEach(valueAllProduct => {
        data.productIDs.forEach(valueProductIDs => {
          if (valueAllProduct.id.toString() === valueProductIDs.productID.toString()
          && valueAllProduct.amount < valueProductIDs.amount ) {
            canNotPayment.push(valueAllProduct.name)
          }
        })
      })

      if(canNotPayment.length !== 0 ){
        return {
          message: `Sản phẩm ${canNotPayment[0]} khộng đủ số lượng theo yêu cầu của quý khách`
        }
      }

      const storeOwner : any = await this.storeOwnersService.findOneStoreOwner({
        _id: userCreatedProduct.storeOwnerID,
      });

      const reqData = await {
        amount: data.totalMoney,
        orderInfo: storeOwner.name
      }

      const dataPaymentMomo = await this.payment_momoService.logData(reqData);


      const payment = await this.paymentService.createOne({
        data: {
          requestId: dataPaymentMomo.body.requestId,
          orderId: dataPaymentMomo.body.orderId,
          payUrl: dataPaymentMomo.body.payUrl,
          signature: dataPaymentMomo.body.signature,
          paymentStatus: PaymentStatus.PENDING,
          ...data
        }
      })

      return {
        message: 'ok',
        payment
      }
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async CreatePaymentHandleDirectPayment({ data }) {
    try {
      if (!data.userCreatedProductID) {
        return {
          message: 'Cần nhập userCreatedProductID nhé 2 bạn'
        }
      }

      if (!data.productIDs || data.productIDs.lenth === 0) {
        return {
          message: 'Cần có sản phẩm để thanh toán'
        }
      }

      const userCreatedProduct = await this.usersService.findOneUser({_id : data.userCreatedProductID})

      if(!userCreatedProduct){
        return {
          message: 'sai ID người tạo ra sản phẩm'
        }
      }

      const allProduct = await this.productsService.findMany({query:{
        createdBy: data.userCreatedProductID
      }})

      var canNotPayment = []

      allProduct.list.forEach(valueAllProduct => {
        data.productIDs.forEach(valueProductIDs => {
          if (valueAllProduct.id.toString() === valueProductIDs.productID.toString()
          && valueAllProduct.amount < valueProductIDs.amount ) {
            canNotPayment.push(valueAllProduct.name)
          }
        })
      })

      if(canNotPayment.length !== 0 ){
        return {
          message: `Sản phẩm ${canNotPayment[0]} khộng đủ số lượng theo yêu cầu của quý khách`
        }
      }

      const payment = await this.paymentService.createOne({
        data: {
          paymentStatus: PaymentStatus.DIRECT_PAYMENT,
          ...data
        }
      })

      data.productIDs.forEach( async (element) => {

        const product : any =  await this.productsService.findProduct({ _id: element.productID });

        await this.productsService.updateOne({
          data:{
            amount : product.amount - element.amount,
            quantitySold: product.quantitySold + element.amount
          },
          query: { _id: element.productID},
        })

        const productisPaying : any =  await this.productsService.findProduct({ _id: element.productID });

        if(productisPaying.amount === 0 ){
          await this.productsService.updateOne({
            data:{
              status: statusProduct.OUT_OF_STOCK
            },
            query: { _id: element.productID},
          })
        }
       });

       const bodyNoti = {
        createdBy: payment.createdBy,
        targetEntityName:'payment',
        targetID:payment._id,
        title:'Một đơn hàng đã thanh toán',
        description:`Một khách hàng đã thanh toán đơn hàng trị giá ${payment.totalMoney}`
       }

       await this.fireBaseService.sendMessageToTopic({
        topic: `User-${data.userCreatedProductID}`,
        notification: {
          body: bodyNoti.description,
          title: bodyNoti.title,
          click_action: bodyNoti.targetID.toString(),
        },
        data: {
          event:bodyNoti.targetEntityName,
          data:bodyNoti.description,
          title:bodyNoti.title,
          clickAction:payment._id.toString(),
        },
      })

      return {
        message: 'ok',
        payment
      }
    } catch (error) {
      return Promise.reject(error);
    }
  }


  async CheckPaymentHandle({ query }) {
    try {
      const payment:any= await this.paymentService.findProduct({'orderId':query.orderId})

      await this.paymentService.updateOne({
        data:{
          title:'Thanh toán thành công',
          paymentStatus: PaymentStatus.PAYMENTED,
          productIDs:payment.productIDs
        },
        query: { _id: payment._id },
      })

      payment.productIDs.forEach( async (element) => {

        const product : any =  await this.productsService.findProduct({ _id: element.productID });

        await this.productsService.updateOne({
          data:{
            amount : product.amount - element.amount,
            quantitySold: product.quantitySold + element.amount
          },
          query: { _id: element.productID},
        })

        const productisPaying : any =  await this.productsService.findProduct({ _id: element.productID });

        if(productisPaying.amount === 0 ){
          await this.productsService.updateOne({
            data:{
              status: statusProduct.OUT_OF_STOCK
            },
            query: { _id: element.productID},
          })
        }
       });

       const bodyNoti = {
        createdBy: payment.createdBy._id,
        targetEntityName:'payment',
        targetID:payment._id,
        title:'Một đơn hàng đã thanh toán',
        description:`Một khách hàng đã thanh toán đơn hàng trị giá ${payment.totalMoney}`
       }

       await this.fireBaseService.sendMessageToTopic({
        topic: `User-${query.userCreatedProductID}`,
        notification: {
          body: bodyNoti.description,
          title: bodyNoti.title,
          click_action: bodyNoti.targetID.toString(),
        },
        data: {
          event:bodyNoti.targetEntityName,
          data:bodyNoti.description,
          title:bodyNoti.title,
          clickAction:payment._id.toString(),
        },
      })

      await this.noticationsService.createOne({
        data: bodyNoti,
      })
      console.log('ok');
      

      return query
    } catch (error) {
      return Promise.reject(error);
    }
  }


}

import { Document, Types } from 'mongoose';

export interface Products extends Document {
  name?: string;
  description?: string;
  photos?: string;
  price?: string;
  origin?: number;
  nextWeight?: String;
  tag?: string;
  inCard?: string;
  percentDiscount?: string;
  quantitySold?: string;
  calories?: string;
  createdBy?: Types.ObjectId;
  storeOwnerID?: Types.ObjectId;
  statusAccount?: string;
  tradeMark?: string;
  amount?: number
  paymentCode?: string;
  combindNumber1?: number
  combindNumber2?: number
  combindString1?: string
  combindString2?: string
  createdAt?: Date;
  updatedAt?: Date;
}

export const fieldNeedToUseRegex = [ 'name' ];

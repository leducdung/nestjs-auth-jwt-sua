import { Injectable } from '@nestjs/common';
import { EmployeesService } from '../employees/employees.service';
import { StoreOwnersService } from '../storeOwners/storeOwners.service';
import { UsersService } from '../users/users.service';
import { Users } from '../users/model/users.interface';
import * as bcrypt from 'bcrypt';
import { Types } from 'mongoose';
import { ProductsService } from "./products.service";
import { FireBaseService } from "../fireBase/firebase.service";
@Injectable()
export class ProductsCombinedService {
  constructor(
    private readonly usersService: UsersService,
    private readonly productsService: ProductsService,
    private readonly fireBaseService:  FireBaseService
  ) {}

async likeAndUnLikeProduct(dataLiking){
    try {

      const user = await this.usersService.findOneUser({
        _id: Types.ObjectId(dataLiking.userID)
      });

       const likeProductIDs = []

       if(!user.productsLiking.includes(dataLiking.productID)){

        likeProductIDs.push(...user.productsLiking)
        likeProductIDs.push(dataLiking.productID)
       }

       const unLikeProductIDs = await user.productsLiking.filter(product => product !== dataLiking.productID)

       if(!user.productsLiking.includes(dataLiking.productID)){

        return  await this.usersService.updateOne({
          data:{
            productsLiking: likeProductIDs
          },
          query: Types.ObjectId(dataLiking.userID),
        })

       }

       return  await this.usersService.updateOne({
        data:{
          productsLiking: unLikeProductIDs
        },
        query: Types.ObjectId(dataLiking.userID),
      })

    } catch (error) {
      return Promise.reject(error);
    }
  }

  async updateAndPushNotification({ data, query , user }){
    try {

      const updatedProduct : any = await this.productsService.updateOne(
        {
          data,
          query: { _id: query._id },
        }
      )

      await this.fireBaseService.sendMessageToTopic({
        topic: `User-${updatedProduct.createdBy}`,
        notification: {
          body: 'data',
          title:  'data.title',
          click_action: '',
        },
        data:{ event : 'buildObjectValueToString(createInAppNotification)'},
      })

      return updatedProduct
    } catch (error) {
      return Promise.reject(error);
    }
  }
}
